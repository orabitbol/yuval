import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CardData } from './card-data.model';
import { RestartDialogComponent } from './components/restart-dialog/restart-dialog.component';
import { HttpClient } from '@angular/common/http';
import { interval } from 'rxjs';
import { player } from './playar';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  cards: CardData[] = [];

  flippedCards: CardData[] = [];

  timer: Number = 0;

  player: player = {
    name: "",
    amount: 0,
    startTime: "",
    endTime: ""
  }

  // playerTwo: player = {
  //   name: "",
  //   amount: 0,
  //   startTime: "",
  //   endTime: ""
  // }




  matchedCount = 0;




  transform(list: Array<any>): Array<any> {
    const newList = [...list];
    newList.sort(() => Math.random() - 0.5);
    return newList;
  }


  constructor(private dialog: MatDialog, private http: HttpClient) {


  }
  post: any[] = [];

  loadPosts() { }

  ngOnInit(): void {
    this.setupCards();
    this.setTimer();
    // let 
    




  }

  setupCards(): void {
    this.http
      .get("https://dev-bot.pico.buzz/memory")
      .subscribe((posts: any) => {
        this.timer = posts.data.time;
        posts.data.images.forEach((image: any, index: Number) => {
          const cardData: CardData = {
            imageId: image.id,
            image: image.image,
            pair_id: image.pair_id,
            state: 'default'

          };
          this.cards.push({ ...cardData });


        })
        this.cards = this.transform(this.cards);
      });
  }

  cardClicked(index: number): void {

    const cardInfo = this.cards[index];
    console.log(cardInfo);

    if (cardInfo.state === 'default' && this.flippedCards.length < 2) {
      cardInfo.state = 'flipped';
      this.flippedCards.push(cardInfo);

      if (this.flippedCards.length > 1) {
        this.checkForCardMatch();
      }

    } else if (cardInfo.state === 'flipped') {
      cardInfo.state = 'default';
      this.flippedCards.pop();

    }
  }

  checkForCardMatch(): void {
    setTimeout(() => {
      const cardOne = this.flippedCards[0];
      const cardTwo = this.flippedCards[1];
      const nextState = cardOne.imageId === cardTwo.pair_id ? 'matched' : 'default';
      cardOne.state = cardTwo.state = nextState;

      this.flippedCards = [];

      if (nextState === 'matched') {
        this.matchedCount++;
        console.log(this.matchedCount);
        console.log(this.cards)

        if (this.matchedCount === this.cards.length / 2) {
          const dialogRef = this.dialog.open(RestartDialogComponent, {
            disableClose: true
          });


          dialogRef.afterClosed().subscribe(() => {
            this.restart();
          });
        }
      }

    }, 1000);
  }

  restart(): void {
    this.matchedCount = 0;
    this.cards = [];
    this.setupCards();
  }

  counter: any

  setTimer(): void {
    let counter = this.timer;
    const interval = setInterval(() => {
      console.log(counter);



    }, 1000)
  }

}

